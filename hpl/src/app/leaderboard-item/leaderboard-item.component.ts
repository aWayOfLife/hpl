import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ITeam } from '../interfaces/team';

@Component({
  selector: 'app-leaderboard-item',
  templateUrl: './leaderboard-item.component.html',
  styleUrls: ['./leaderboard-item.component.css']
})
export class LeaderboardItemComponent implements OnInit {
  @Input() team:ITeam;
  @Output() teamSelected = new EventEmitter<ITeam>();
  count:number =5;
  points:number[]=[];
  constructor() { }

  ngOnInit(): void {
    if(this.team.match[0]?.form!=null){
      this.count = (5-this.team.match[0]?.form?.length)>0?(5-this.team.match[0]?.form?.length):0
      this.points = this.team.match[0]?.form.slice(-5)
    }

  }

  checkForMatch(){
    
      this.teamSelected.emit(this.team);
  }

}
