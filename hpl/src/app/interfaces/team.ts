import { IMatch } from "./match";

export interface ITeam {
    id: string;
    teamName: string;
    wins: number;
    losses: number;
    ties: number;
    score: number;
    position: number;
    isChecked: boolean;
    match: IMatch[];
  }