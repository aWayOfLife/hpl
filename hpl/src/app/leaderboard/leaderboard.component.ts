import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { ITeam } from '../interfaces/team';
import { ToastrService } from 'ngx-toastr';
import { IResult } from '../interfaces/result';



@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.css']
})
export class LeaderboardComponent implements OnInit {
  saveResult:IResult[] =[];
  teams:ITeam[];
  p: number = 1;
  teamName:string='';
  selectedTeams: ITeam[] = [];
  matchResult: boolean[] = [false,false,false]
  isMatch:boolean = false;
  isTeamSort:boolean = false;
  isPointSort:boolean = false;
  filter:ITeam ={id:null,teamName:'',wins:null,losses:null,ties:null,score:null,position:null,isChecked:null, match:null};
  constructor(private appService:AppService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.getAllTeams();
  }

  teamSelected(team:ITeam){
    if(this.isMatch){
      return;
    }
    if(this.selectedTeams?.length===2){
      //reset is checked of items and then remove the items
      this.selectedTeams.map(r=>r.isChecked=false);
      this.selectedTeams=[];
    }
    else if(team.isChecked === false || team.isChecked === null){
      team.isChecked = true;
      this.selectedTeams.push(team);
    }
    else{
      // remove team from array
      team.isChecked = false;
      this.selectedTeams.filter(r=>r.id = team.id)
    }
    //console.log(this.selectedTeams);
  }

  match(){
    if(this.selectedTeams?.length===2){
      //open modal
      console.log("true");
      this.isMatch = true;
      this.matchResult=[false, false, false];
    }
    else{
      console.log("false");
      this.toastr.success('please select two teams','Teams not selected',{
        positionClass:'toast-bottom-right'
      });
    }
  }

  result(option:number){
    this.matchResult = this.matchResult.map((data,index)=>{
      if(index===option){
        data = true;
      }
      else{
        data = false;
      }
      return data;
    })
    console.log(this.matchResult);
  }

  saveMatch(){
    for(let i = 0; i<2; i++)
    {
      this.saveResult[i] = {id:this.selectedTeams[i].id, point:(this.matchResult[i]?3:(this.matchResult[2]?1:0))}
    }
    this.appService.saveResult(this.saveResult).subscribe(result =>{
      if(result){
        this.toastr.success('Updating table, please wait..','Match Saved',{
          positionClass:'toast-bottom-right'
        });
        this.getAllTeams();
      }
    },error=>{
      console.log(error);
    })
    
    this.isMatch=false;
    this.selectedTeams.map(r=>r.isChecked=false);
    this.selectedTeams=[];
    this.saveResult = [];
    
  }

  cancelMatch(){
    this.isMatch=false;
  }

  addTeam(){
    if(this.teamName===''){
      this.toastr.success('please enter team name','Enter Team Name',{
        positionClass:'toast-bottom-right'
      });
      console.log("neame empty")
     
    }
    else{
      //add to db
      this.appService.addTeam(this.teamName).subscribe(result =>{
        if(result){
          this.toastr.success(this.teamName+' was added','Team Added',{
            positionClass:'toast-bottom-right'
          });
          this.getAllTeams();
        }
      },error =>{
        console.log(error);
        this.toastr.success(this.teamName+' was not added','Team Not Added',{
          positionClass:'toast-bottom-right'
        });
      })
    }
  }

  getAllTeams(){
    this.appService.getAllTeams()
    .pipe()
    .subscribe(result=>{
      this.teams = result.map(this.addPosition);
    },
    error=>{
      console.log(error);
    }, ()=>{
      this.teamName = '';
    })
  }

  sort(basis:string){
    switch (basis){
      case 'team':
        if(!this.isTeamSort){
          this.teams = this.teams.sort((a,b)=> {return a.teamName.localeCompare(b.teamName)});
          this.isTeamSort = true;
        }else{
          this.teams = this.teams.sort((a,b)=> {return a.teamName.localeCompare(b.teamName)}).reverse();
          this.isTeamSort = false;
        }
        break;
      
      case 'points':
        if(!this.isPointSort){
          this.teams = this.teams.sort((a,b)=> {return a.score - b.score}).reverse();
          this.isPointSort = true;
        }else{
          this.teams = this.teams.sort((a,b)=> {return a.score - b.score});
          this.isPointSort = false;
        }
        break;
    }
  }

  addPosition(element, index, array):ITeam{
    element.position = index+1;
    element.isChecked = null;
    return element;
  }

  onKeyUpEvent(event: any){

    if(event.target.value!=''){
      this.p=1;
    }

  }
}
