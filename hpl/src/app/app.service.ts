import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ITeam } from './interfaces/team';
import { IResult } from './interfaces/result';


@Injectable({
  providedIn: 'root'
})
export class AppService {
  baseUrl:string = 'https://hacker-pl.herokuapp.com/api/hacker';
  // baseUrl:string = 'https://localhost:44335/api/hacker';

  constructor(private http:HttpClient) { }

  getAllTeams():Observable<ITeam[]>{
    return this.http.get<ITeam[]>(this.baseUrl);
  }

  saveResult(saveResult:IResult[]):Observable<boolean>{
    return this.http.post<boolean>(this.baseUrl,saveResult);
  }

  addTeam(teamName: string):Observable<boolean>{
    return this.http.post<boolean>(this.baseUrl+'/'+teamName,null);
  }
}
