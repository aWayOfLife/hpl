// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyBuCrzbdPhtWta42XUPVnYlsNYp7SAV3Cc",
    authDomain: "hacker-premier-league.firebaseapp.com",
    projectId: "hacker-premier-league",
    storageBucket: "hacker-premier-league.appspot.com",
    messagingSenderId: "135248271917",
    appId: "1:135248271917:web:cd5b0b971ef7044e9db6a4"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
