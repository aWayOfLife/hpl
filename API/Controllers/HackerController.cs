﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Driver;

namespace API
{
    [ApiController]
    [Route("api/[controller]")]

    public class HackerController : ControllerBase
    {
        private IMongoCollection<Team> teamsCollection;
        private IMongoCollection<Match> matchesCollection;
        public HackerController(IMongoClient client)
        {
            var database = client.GetDatabase("HPL");
            teamsCollection = database.GetCollection<Team>("teams");
            matchesCollection = database.GetCollection<Match>("matches");
        }

        [HttpGet]
        public async Task<ActionResult<IReadOnlyList<Team>>> GetAllTeams()
        {

            //List<Team> teams = await teamsCollection.Find(_ => true).SortByDescending(t => t.Score).ToListAsync();

            List<Team> teams = await teamsCollection
                .Aggregate()
                .Lookup(
                    matchesCollection,
                    t => t.Id,
                    m => m.Id,
                    (Team tm)=>tm.Match
                    )
                .SortByDescending(t => t.Score)
                .ToListAsync();
           


            return Ok(teams);
            
        }

        [HttpPost]
        [Route("{teamName}")]
        public async Task<ActionResult<bool>> AddTeam(string teamName)
        {
            Team team = new Team();
            team.TeamName = teamName;
            team.Wins = 0;
            team.Losses = 0;
            team.Ties = 0;
            team.Score = 0;

            await teamsCollection.InsertOneAsync(team);

            return Ok(true);

        }

        [HttpPost]
        public async Task<ActionResult<bool>> SaveResult(Result[] result)
        {
            foreach(Result r in result)
            {
                var filterMatch = Builders<Match>.Filter.Eq(m => m.Id, r.Id);
                Match match = await matchesCollection.Find(filterMatch).FirstOrDefaultAsync();
                if (match != null)
                {
                    //match.Form.Append(r.Point);
                    var matchUpdate = Builders<Match>.Update
                        .Set(m => m.Form, match.Form.Append(r.Point));
                        
                    await matchesCollection.UpdateOneAsync(filterMatch, matchUpdate);
                }
                else
                {
                    Match newMatch = new Match();
                    newMatch.Id = r.Id;
                    newMatch.Form = new int[] { r.Point };
                    await matchesCollection.InsertOneAsync(newMatch);
                }

                var filterTeam = Builders<Team>.Filter.Eq(t => t.Id, r.Id);
                if(r.Point == 3)
                {
                    var teamUpdate = Builders<Team>.Update.Inc("wins", 1).Inc("score",3); ;
                    await teamsCollection.UpdateOneAsync(filterTeam, teamUpdate);

                }
                else if(r.Point == 1)
                {
                    var teamUpdate = Builders<Team>.Update.Inc("ties", 1).Inc("score",1);
                    await teamsCollection.UpdateOneAsync(filterTeam, teamUpdate);

                }
                else
                {
                    var teamUpdate = Builders<Team>.Update.Inc("losses", 1);
                    await teamsCollection.UpdateOneAsync(filterTeam, teamUpdate);
                }


            }
            return Ok(true);
        }

        
    }
}
