﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Model
{
    public class Team
    {
        
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("team_name")]
        public string TeamName { get; set; }

        [BsonElement("wins")]
        public int Wins { get; set; }

        [BsonElement("losses")]
        public int Losses { get; set; }

        [BsonElement("ties")]
        public int Ties { get; set; }

        [BsonElement("score")]
        public int Score { get; set; }

        public Match[] Match { get; set; }
    }
}
